use Mix.Config

config :sinatra,
  ecto_repos: [ElixirSinatra.Repo]

import_config "#{Mix.env}.exs"
