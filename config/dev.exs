use Mix.Config

config :sinatra, ElixirSinatra.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "sinatra_dev",
  hostname: "postgres",
  pool_size: 10
