use Mix.Config

config :sinatra, ElixirSinatra.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "sinatra_test",
  hostname: "postgres",
  pool: Ecto.Adapters.SQL.Sandbox

config :logger, level: :info
