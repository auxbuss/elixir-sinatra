defmodule ElixirSinatraTest do
  use ExUnit.Case, async: true
  use Plug.Test
  import ElixirSinatra.TestHelpers

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(ElixirSinatra.Repo)
  end

  test 'saying hello' do
    response = conn(:get, "/hello") |> ElixirSinatra.call({})
    assert response.status == 200
    assert response.resp_body == "Hello, world!"
  end

  test 'fail at make a user request' do
    response = conn(:get, "/user") |> ElixirSinatra.call({})
    assert response.status == 404
    assert response.resp_body == "Couldn't find that page, sorry!"
  end

  test 'make a user request' do
    user = insert_user()
    response = conn(:get, "/user/#{user.id}") |> ElixirSinatra.call({})
    assert response.status == 200
    assert String.contains? response.resp_body, "Looks like you've requested information for the user with id #{user.id}."
    assert String.contains? response.resp_body, "Also, let me just say that Marc is awesome."
  end
end
