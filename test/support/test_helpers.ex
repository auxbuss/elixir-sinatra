defmodule ElixirSinatra.TestHelpers do
  alias ElixirSinatra.Repo

  def insert_user(attrs \\ %{}) do
    changes = Map.merge(%{
      first_name: "Marc",
      last_name: "Cooper",
    }, attrs)

    %ElixirSinatra.User{}
    |> ElixirSinatra.User.changeset(changes)
    |> Repo.insert!()
  end
end
