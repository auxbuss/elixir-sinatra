defmodule ElixirSinatra.Router do
  require EEx
  alias ElixirSinatra.User

  def route("GET", ["hello"], conn) do
    # this route is for /hello
    conn |> Plug.Conn.send_resp(200, "Hello, world!")
  end

  EEx.function_from_file :defp, :template_show_user, "lib/sinatra/web/templates/show_user.html.eex", [:user_id, :user_name, :user_surname]

  def route("GET", ["user", user_id], conn) do
    # this route is for /users/<user_id>
    case ElixirSinatra.Repo.get(User, user_id) do
      nil ->
        conn
        |> Plug.Conn.send_resp(404, "User with that ID not found, sorry")
      user ->
        page_contents = template_show_user(user.id, user.first_name, user.last_name)
        conn
        |> Plug.Conn.put_resp_content_type("text/html")
        |> Plug.Conn.send_resp(200, page_contents)
    end
  end

  def route(_method, _path, conn) do
    # this route is called if no other routes match
    conn |> Plug.Conn.send_resp(404, "Couldn't find that page, sorry!")
  end
end
