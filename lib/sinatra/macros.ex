defmodule ElixirSinatra.Macros do

  defmacro __using__(_opts) do
    quote do
      def init(options) do
        IO.puts "ElixirSinatra is taking to the stage..."
        options
      end

      def call(conn, _opts) do
        route(conn.method, conn.path_info, conn)
      end
    end
  end
end
