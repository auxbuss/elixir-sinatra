defmodule ElixirSinatra do
  import ElixirSinatra.Router
  use ElixirSinatra.Macros

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      supervisor(ElixirSinatra.Repo, []),
    ]

    opts = [strategy: :one_for_one, name: ElixirSinatra.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
